import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import { Banner, Logo } from '../../assests';

export default function Header() {
    const page = localStorage.getItem('page')
    const [isOpen, setIsOpen] = useState(false)
    console.log('page :', page)
    
    return (
        <>
            <nav className="sm:hidden bg-gray-900 fixed top-0 border-gray-200 px-2 sm:px-4 py-2.5 w-full">
                <div className="container flex flex-wrap items-center justify-between mx-auto">
                    <a href="https://flowbite.com/" className="flex items-center">
                        <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">Spices</span>
                    </a>
                    <button onClick={() => setIsOpen(!isOpen)} type="button" className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-default" aria-expanded="false">
                        <span className="sr-only">Open main menu</span>
                        <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path></svg>
                    </button>
                    <div className={`${isOpen ? 'md:block' : 'hidden'} w-full md:w-auto" id="navbar-default`}>
                        <ul className="flex flex-col p-4 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                            <li>
                            <Link href="/about" className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:border-0 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">About</Link>
                            </li>
                            <li>
                            <Link href="/journey" className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:border-0 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Journey</Link>
                            </li>
                            <li>
                            <Link href="/feature" className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:border-0 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Feature</Link>
                            </li>
                            <li>
                            <Link href="/product" className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:border-0 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Product</Link>
                            </li>
                            <li>
                            <Link href="/team" className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:border-0 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Team</Link>
                            </li>
                            <li>
                            <Link href="/contact" className="block py-2 pl-3 pr-4 text-gray-700 rounded hover:bg-gray-100 md:border-0 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white">Contact</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <nav className={`hidden sm:block w-full h-fit bg-black`}>
                <div className='flex items-center'>
                    <div className='absolute px-12 gap-5 right-0 text-right hidden md:block'>
                        <h3 className='text-sm md:text-2xl font-semibold tracking-normal'>Supplying across the globe</h3>
                        <h1 className='text-xl md:text-4xl font-semibold tracking-wide'>The Leading Supplier <br/> of Coconut Product</h1>
                    </div>
                    <Image
                        src={Banner}
                        alt='spices-logo'
                        objectFit='cover'
                    />
                    <div className='flex justify-between absolute top-0  w-full px-6 md:px-12 pt-2 md:pt-4'>
                        <section >
                            <Image
                                src={Logo}
                                alt='spices-logo'
                                className='h-8 w-20'
                            />
                        </section>
                        <section className='hidden md:block'>
                            <ul className='flex gap-5'>
                                <li>
                                    <Link href="/about" className={`${page === 'about' && 'bg-white py-1 px-3 bg-opacity-50 rounded-full'}`}>About</Link>
                                </li>
                                <li>
                                    <Link href="/journey" className={`${page === 'journey' && 'bg-white py-1 px-3 bg-opacity-50 rounded-full'}`}>Journey</Link>
                                </li>
                                <li>
                                    <Link href="/feature" className={`${page === 'feature' && 'bg-white py-1 px-3 bg-opacity-50 rounded-full'}`}>Feature</Link>
                                </li>
                                <li>
                                    <Link href="/product" className={`${page === 'product' && 'bg-white py-1 px-3 bg-opacity-50 rounded-full'}`}>Product</Link>
                                </li>
                                <li>
                                    <Link href="/team" className={`${page === 'team' && 'bg-white py-1 px-3 bg-opacity-50 rounded-full'}`}>Team</Link>
                                </li>
                                <li>
                                    <Link href="/contact" className={`${page === 'contact' && 'bg-white py-1 px-3 bg-opacity-50 rounded-full'}`}>Contact</Link>
                                </li>
                            </ul>
                        </section>
                    </div>
                </div>
            </nav>
        </>
    )
}
