import { useEffect } from 'react'
import Layout from '../../layout'

export default function Contract() {
  useEffect(() => {
    localStorage.page = 'contact'
  },[])
  return (
    <Layout>
        <main className='py-5 md:py-10 min-h-screen'>
            Contract Page
        </main>
    </Layout>
  )
}
