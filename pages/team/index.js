import Layout from '../../layout'

export default function Team() {
  useEffect(() => {
    localStorage.page = 'team'
  },[])
  return (
    <Layout>
        <main className=' flex-1 py-5 md:py-10 min-h-screen'>
            Team Page
        </main>
    </Layout>
  )
}
