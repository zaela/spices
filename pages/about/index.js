import { useEffect } from 'react'
import { BsChevronRight } from 'react-icons/bs'
import Layout from '../../layout'

export default function About() {
  useEffect(() => {
    localStorage.page = 'about'
  },[])
  return (
    <Layout>
        <main className='min-h-screen py-5 md:py-8'>
          <div className='px-8 md:px-14'>
            <h2 className='text-gray-800 text-xl font-bold'>About Page</h2>
          </div>
          <div className='hidden px-8 md:px-14'>
            <h2 className='text-gray-800 text-xl font-bold'>About Page</h2>
            <div className='h-4' />
            <ul role="list">
                <li className="group/list hover:bg-slate-100 flex justify-between gap-5 p-5">
                  <div className='flex gap-5'>
                    <img className='w-14 h-14' src="https://png.pngtree.com/png-clipart/20190520/original/pngtree-vector-users-icon-png-image_4144740.jpg" alt="" />
                    <div className='flex-col gap-2'>
                      <a href="/about" className='text-black'>zaela</a>
                      <p className='text-gray-400'>Frontend Developer</p>
                    </div>
                  </div>
                  <a className="group/edit invisible hover:bg-slate-200 group-hover/list:visible flex items-center px-4 py-2 rounded-full" href="/about">
                    <span className="group-hover/edit:text-gray-700 group-hover/list:text-gray-500 text-md font-semibold">Call</span>
                    <BsChevronRight className="group-hover/edit:translate-x-2 group-hover/edit:text-slate-500 text-gray-500 text-md font-semibold transition-transform"/>
                  </a>
                </li>
            </ul>
            <div className='h-4' />
            <h2 className='text-gray-800 text-xl font-bold'>About Page</h2>
            <div className='w-full bg-gray-400 p-6'>
              <div className='w-fit group/button'>
                <button className='border-4 border-white py-2 px-4 group-hover/button:bg-white group-hover/button:bg-opacity-20 cursor-pointer group-hover/button:border-none transition-all duration-300'>
                  <span className='text-md font-semibold text-white group-hover/button:text-white group-hover/button:text-opacity-50 transition-all duration-300'>Our Products</span> 
                </button>
              </div>
            </div>
            <div className='h-4' />
            <h2 className='text-gray-800 text-lg font-bold'>Rotation</h2>
            <div className='h-4' />
            <div className='w-40 h-40 bg-pink-500 rouded-xl group/item'>
              <div className='w-full h-full bg-sky-500 rouded-lg shadow-lg group-hover/item:scale-120 
              group-hover/item:-rotate-180  group-hover/item:bg-pink-500 rounded-lg origin-top-right transition-all'>
                {/* hover:translate-x-16 */}
              </div>
            </div>
            <div className='h-4' />
            <h2 className='text-gray-800 text-lg font-bold'>Rotation and Duration</h2>
            <div className='h-4' />
            <div className='w-40 h-40 bg-pink-500 rouded-xl group/item'>
              <div className='w-full h-full bg-sky-500 rouded-lg shadow-lg group-hover/item:scale-120 
              group-hover/item:-rotate-180  group-hover/item:bg-pink-500 rounded-lg origin-top-right transition-all duration-1000'>
                {/* hover:translate-x-16 */}
                {/* delay-1000 */}
                {/* ease-in */}
              </div>
            </div>
            <div className='h-4' />
            <h2 className='text-gray-800 text-lg font-bold'>Animations</h2>
            <div className='h-4' />
            <div className='w-full flex gap-16'>
              <div className='w-40 h-40 bg-pink-500 rouded-xl animate-spin'/>
              <div className='w-40 h-40 bg-orange-500 rouded-full animate-ping'/>
              <div className='w-40 h-40 bg-red-500 rouded-xl animate-pulse'/>
              <div className='w-40 h-40 bg-green-500 rouded-xl animate-bounce'/>
            </div>
          </div>
        </main>
    </Layout>
  )
}
