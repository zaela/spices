import { useEffect } from 'react'
import Layout from '../../layout'

export default function Product() {
  useEffect(() => {
    localStorage.page = 'product'
  },[])
  return (
    <Layout>
        <main className='py-5 md:py-10 min-h-screen'>
            Product Page
        </main>
    </Layout>
  )
}
