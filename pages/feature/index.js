import { useEffect } from 'react'
import Layout from '../../layout'

export default function Feature() {
  useEffect(() => {
    localStorage.page = 'feture'
  },[])
  return (
    <Layout>
        <main className='py-5 md:py-10 min-h-screen'>
            Feature Page
        </main>
    </Layout>
  )
}
