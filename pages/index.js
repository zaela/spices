import Image from 'next/image'
import { Commitment, Innovation, Integrity, Product, Quality, Team1, Team2, Team3 } from '../assests'
import Layout from '../layout'
import styles from '../styles/Home.module.css'
import { BsArrowLeftCircle, BsArrowRightCircle, BsInstagram, BsLinkedin } from "react-icons/bs"

export default function Home() {
  useEffect(() => {
    localStorage.removeItem('page')
  },[])
  return (
    <Layout>
      <main className='py-5 md:py-10'>
        <section id='about-us' className="h-fit px-8 md:px-14 grid md:grid-cols-2 gap-9 my-4 md:my-7">
          <div className='text-black order-2 md:order-1'>
            <h2 className='text-xl md:text-2xl font-semibold'>About Us</h2>
            <p className='text-sm md:text-lg mt-2'>
              Universal Coco Indonesia is the leading supplier of coconut products from Indonesia. We are a 
              company engaged in manufacturing various coconut products with high-quality materials. We process 
              more than 1,500 hectares of coconut plantations, spread across several islands in Eastern Indonesia
            </p>
            <p className='text-sm md:text-lg mt-2'>
              Universal Coco Indonesia is the leading supplier of coconut products from Indonesia. We are a 
              company engaged in manufacturing various coconut products with high-quality materials. We process 
              more than 1,500 hectares of coconut plantations, spread across several islands in Eastern Indonesia
            </p>
          </div>
          <div className='flex justify-end order-1 md:order-2'>
            <Image 
              src={Product}
              alt='best spices from indonesia'
              title='spices'
              className='rounded-md'
              priority
            />
          </div>
        </section>

        <section id='title-features' className='text-center px-8 md:px-14 text-black text-xl md:text-3xl font-semibold my-16 mt-[5rem]'>
          Our Key Features
        </section>
        <section id='features' className="px-14 h-fit grid md:grid-cols-2 gap-9 my-7">
          <div className='text-black'>
            <div className="w-full flex justify-center">
              <Image src={Integrity} alt='best-spices-indonesian-team' className="h-44 w-44"/>
            </div>
            <h2 className='text-lg md:text-xl text-center font-semibold'>Integrity</h2>
            <p className='text-sm md:text-lg mt-2 text-center'>
            Integrity means telling the truth, keeping our word, and treating others with fairness and respect. 
            Integrity is one of our most cherished assets. It must not be compromised.
            </p>
          </div>
          <div className='text-black'>
            <div className="w-full flex justify-center">
              <Image src={Quality} alt='best-spices-indonesian-team' className="h-44 w-44"/>
            </div>
            <h2 className='text-lg md:text-xl text-center font-semibold'>Quality</h2>
            <p className='text-sm md:text-lg mt-2 text-center'>
            Quality is exhibited in many ways by selling and supporting products and services that delight customers, 
            establishing a work environment, and delivering financial results that meet investor expectations.
            </p>
          </div>
          <div className='text-black'>
            <div className="w-full flex justify-center">
              <Image src={Commitment} alt='best-spices-indonesian-team' className="h-44 w-44"/>
            </div>
            <h2 className='text-lg md:text-xl text-center font-semibold'>Commitment</h2>
            <p className='text-sm md:text-lg mt-2'>
            Integrity means telling the truth, keeping our word, and treating others with fairness and respect. 
            Integrity is one of our most cherished assets. It must not be compromised.
            </p>
          </div>
          <div className='text-black'>
            <div className="w-full flex justify-center">
              <Image src={Innovation} alt='best-spices-indonesian-team' className="h-44 w-44"/>
            </div>
            <h2 className='text-lg md:text-xl text-center font-semibold'>Innovation</h2>
            <p className='text-sm md:text-lg mt-2 text-center'>
            Quality is exhibited in many ways by selling and supporting products and services that delight customers, 
            establishing a work environment, and delivering financial results that meet investor expectations.
            </p>
          </div>
        </section>
        <section id='product' className="bg-gray-200 px-8 md:px-14 py-7 h-fit mb-7 mt-[6rem] grid md:grid-cols-12">
          <div className='w-full flex justify-start items-center'>
            <BsArrowLeftCircle className="h-8 w-8 text-black hidden md:block"/> 
          </div>
          <div className='grid md:grid-cols-2 col-span-10 gap-9'>
            <div className='text-black order-2 md:order-1'>
              <h2 className='text-xl md:text-2xl font-semibold'>Our Product</h2>
              <h2 className='text-lg md:text-xl font-semibold mt-2'>Cengkeh</h2>
              <p className='text-sm md:text-lg mt-2'>
                Universal Coco Indonesia is the leading supplier of coconut products from Indonesia. We are a 
                company engaged in manufacturing various coconut products with high-quality materials. We process 
                more than 1,500 hectares of coconut plantations, spread across several islands in Eastern Indonesia
              </p>
              <p className='text-sm md:text-lg mt-2'>
                Universal Coco Indonesia is the leading supplier of coconut products from Indonesia. We are a 
                company engaged in manufacturing various coconut products with high-quality materials. We process 
                more than 1,500 hectares of coconut plantations, spread across several islands in Eastern Indonesia
              </p>
            </div>
            <div className='flex justify-end order-1 md:order-2'>
              <Image 
                src={Product}
                alt='best spices from indonesia'
                className='h-72 w-fit rounded-md'
              />
            </div>
          </div>
          <div className='w-full flex justify-end items-center'>
            <BsArrowRightCircle className="h-8 w-8 text-black hidden md:block"/>
          </div>

          <div className='w-full flex justify-end gap-4 items-center'>
            <BsArrowLeftCircle className="h-6 md:h-8 w-6 md:w-8 mt-4 text-black md:hidden"/>
            <BsArrowRightCircle className="h-6 md:h-8 w-6 md:w-8 mt-4 text-black md:hidden"/>
          </div>
        </section>
        <section id='teams' className='text-center px-8 md:px-14 text-black text-2xl md:text-3xl font-semibold my-16 mt-[5rem]'>
          Our Teams
        </section>
        <section id='teams' className="px-8 md:px-14 py-7 h-fit mb-7 mt-2 grid md:grid-cols-3">
          <div className='w-full justify-center p-5'>
            <div className='w-full flex justify-center mb-3'>
              <div className="rounded-[100%] h-28 md:h-44 w-28 md:w-44 bg-black overflow-clip">
                <Image src={Team2} alt='best-spices-indonesian-team' className="w-full"/>
              </div>
            </div>
            <div className='text-center text-black'>
              <p className='text-lg md:text-xl'>Agus</p>
              <p className='text-lg md:text-xl font-semibold'>Founder</p> 
              <p className='text-sm md:text-lg'>Our faith is the substance of our future. There is no big success without big sacrifice.</p>
            </div>
            <div className='w-full flex gap-2 justify-center text-gray-700 mt-4'>
                <BsLinkedin className="w-5 h-5"/>
                <BsInstagram className="w-5 h-5"/>
            </div>
          </div>
          <div className='w-full justify-center p-5'>
            <div className='w-full flex justify-center mb-3'>
              <div className="rounded-[100%] h-28 md:h-44 w-28 md:w-44 bg-black overflow-clip">
                <Image src={Team2} alt='best-spices-indonesian-team' className="bg-cover"/>
              </div>
            </div>
            <div className='text-center text-black'>
              <p className='text-lg md:text-xl'>Agus</p>
              <p className='text-lg md:text-xl font-semibold'>Co-Founder</p> 
              <p className='text-sm md:text-lg'>Our faith is the substance of our future. There is no big success without big sacrifice.</p>
            </div>
            <div className='w-full flex gap-2 justify-center text-gray-700 mt-4'>
                <BsLinkedin className="w-5 h-5"/>
                <BsInstagram className="w-5 h-5"/>
            </div>
          </div>
          <div className='w-full justify-center p-5'>
            <div className='w-full flex justify-center mb-3'>
              <div className="rounded-[100%] h-28 md:h-44 w-28 md:w-44 bg-black overflow-clip">
                <Image src={Team2} alt='best-spices-indonesian-team' className="w-full"/>
              </div>
            </div>
            <div className='text-center text-black'>
              <p className='text-lg md:text-xl'>Agus</p>
              <p className='text-lg md:text-xl font-semibold'>Business Development</p> 
              <p className='text-sm md:text-lg'>Our faith is the substance of our future. There is no big success without big sacrifice.</p>
            </div>
            <div className='w-full flex gap-2 justify-center text-gray-700 mt-4'>
                <BsLinkedin className="w-5 h-5"/>
                <BsInstagram className="w-5 h-5"/>
            </div>
          </div>
        </section>
      </main>
    </Layout>
  )
}
