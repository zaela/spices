import { useEffect } from 'react'
import Layout from '../../layout'

export default function Journey() {
  useEffect(() => {
    localStorage.page = 'journey'
  },[])
  return (
    <Layout>
        <main className='py-5 md:py-10 min-h-screen'>
            Journey Page
        </main>
    </Layout>
  )
}
